import React, { useContext } from "react";
import { useAuth } from "../store";

const Index = () => {
  const { user, login, logout } = useAuth();
  console.log(user);

  return (
    <div className="flex flex-col bg-blue-400">
      {" "}
      {user && "quiz"}
      <button onClick={() => login()}>login</button>
      <button onClick={() => logout()}>logout</button>
    </div>
  );
};

export default Index;
